# NASA Project

#### To make it work, call endpoint
> POST /api/v1/rover

with payload : 
```json
{ 
  "mars": 
      {
        "x": 5,
        "y": 5
      },
  "rovers":
    [
      {
        "x": 1,
        "y": 2,
        "orientation": "N",
        "instructions": "LMLMLMLMM"
      },
      {
        "x": 3,
        "y": 3,
        "orientation": "E",
        "instructions": "MMRMMRMRRM"
      }
    ]
}
```