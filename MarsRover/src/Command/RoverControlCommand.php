<?php
namespace App\Command;

use App\UI\Controller\RoverController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RoverControlCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'rover:control';

    /** @var RoverController */
    private $controller;


    public function __construct(RoverController $controller)
    {
        parent::__construct();
        $this->controller = $controller;
    }

    protected function configure()
    {
        $this->setDescription('Get all Basis rewards.')
            ->setHelp('This command allows you to get every rewards on Basis API.');

        $this->addArgument('plateauMaxX', InputArgument::REQUIRED, 'Plateau max X coordinate');
        $this->addArgument('plateauMaxY', InputArgument::REQUIRED, 'Plateau max Y coordinate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $plateauMaxX = $input->getArgument('plateauMaxX');
        $plateauMaxY = $input->getArgument('plateauMaxY');

        $output->writeln([
            'Welcome on Mars',
            '============',
            "Creation of your plateau ($plateauMaxX;$plateauMaxY)",
        ]);

        // new Tableau

        $output->writeln([
            'Please, give us the upper-right coordinates of your plateau',
        ]);

        try {
            //$this->controller->roverControl();
            $output->writeln('Done :)');
            return Command::SUCCESS;

        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }


    }
}
