<?php
declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\ValueObject\Coordinates;

/**
 * @author Julie Boisnard <contact@julie-boisnard.fr>
 */
class Mars
{
    /** @var Coordinates */
    private $coordinates;

    public function __construct(Coordinates $coordinates)
    {
        $this->coordinates = $coordinates;
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }
}
