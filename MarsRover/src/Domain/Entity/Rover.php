<?php
declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\ValueObject\Coordinates;

/**
 * @author Julie Boisnard <contact@julie-boisnard.fr>
 */
class Rover
{
    const AVAILABLE_MOVES = [
        self::MOVE_LEFT,
        self::MOVE_RIGHT
    ];

    const AVAILABLE_ORIENTATIONS = [
        self::ORIENTATION_NORTH,
        self::ORIENTATION_EAST,
        self::ORIENTATION_SOUTH,
        self::ORIENTATION_WEST
    ];

    const MOVE_LEFT = 'L';
    const MOVE_RIGHT = 'R';
    const ORIENTATION_NORTH = 'N';
    const ORIENTATION_EAST = 'E';
    const ORIENTATION_SOUTH = 'S';
    const ORIENTATION_WEST = 'W';

    /** @var Coordinates */
    private $coordinates;

    /** @var string */
    private $orientation;

    public function __construct(Coordinates $coordinates, string $orientation)
    {
        $this->coordinates = $coordinates;
        $this->orientation = $orientation;
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    public function getOrientation(): string
    {
        return $this->orientation;
    }

    public function changeOrientation(string $move): void
    {
        switch ($this->orientation) {
            case self::ORIENTATION_NORTH:
                if (self::MOVE_LEFT === $move) {
                    $this->orientation = self::ORIENTATION_WEST;
                } else {
                    $this->orientation = self::ORIENTATION_EAST;
                }
                break;
            case self::ORIENTATION_EAST:
                if (self::MOVE_LEFT === $move) {
                    $this->orientation = self::ORIENTATION_NORTH;
                } else {
                    $this->orientation = self::ORIENTATION_SOUTH;
                }
                break;
            case self::ORIENTATION_SOUTH:
                if (self::MOVE_LEFT === $move) {
                    $this->orientation = self::ORIENTATION_EAST;
                } else {
                    $this->orientation = self::ORIENTATION_WEST;
                }
                break;
            case self::ORIENTATION_WEST:
                if (self::MOVE_LEFT === $move) {
                    $this->orientation = self::ORIENTATION_SOUTH;
                } else {
                    $this->orientation = self::ORIENTATION_NORTH;
                }
                break;
        }
    }

    public function move(): void
    {
        switch ($this->orientation) {
            case self::ORIENTATION_NORTH:
                $this->moveNorth();
                break;
            case self::ORIENTATION_EAST:
                $this->moveEast();
                break;
            case self::ORIENTATION_SOUTH:
                $this->moveSouth();
                break;
            case self::ORIENTATION_WEST:
                $this->moveWest();
                break;
        }
    }

    public function moveNorth(): void
    {
        $this->coordinates = new Coordinates(
            $this->coordinates->getX(),
            $this->coordinates->getY()+1
        );
    }

    public function moveEast(): void
    {
        $this->coordinates = new Coordinates(
            $this->coordinates->getX()+1,
            $this->coordinates->getY()
        );
    }

    public function moveSouth(): void
    {
        $this->coordinates = new Coordinates(
            $this->coordinates->getX(),
            $this->coordinates->getY()-1
        );
    }

    public function moveWest(): void
    {
        $this->coordinates = new Coordinates(
            $this->coordinates->getX()-1,
            $this->coordinates->getY()
        );
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return sprintf('%s %s %s', $this->getCoordinates()->getX(), $this->getCoordinates()->getY(), $this->getOrientation());
    }
}
