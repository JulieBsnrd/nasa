<?php
declare(strict_types=1);

namespace App\Exceptions;


/**
 * @author Julie Boisnard <contact@julie-boisnard.fr>
 */
class InvalidPositionException extends \LogicException
{
    /**
     * @throws InvalidOrientationException
     * @param mixed $x
     * @param mixed $y
     */
    public static function throw($x, $y): void
    {
        throw new self("Position \"$x\";\"$y\" doesnt exist.");
    }
}
