<?php
declare(strict_types=1);

namespace App\UI\Controller;

use App\Domain\Entity\Mars;
use App\Domain\Entity\Rover;
use App\Domain\ValueObject\Coordinates;
use App\Exceptions\InvalidMoveException;
use App\Exceptions\InvalidOrientationException;
use App\Exceptions\InvalidPositionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class RoverController
{
    /**
     * @Route("/api/v1/rover")
     * @param Request $request
     * @return Response
     */
    public function roverControl(Request $request): Response
    {
        $newRoversPositions = '';

        $data = $this->extractData($request);

        if (!isset($data['mars'])) {
            throw new BadRequestHttpException('Missing node "mars"');
        }

        if (!isset($data['rovers'])) {
            throw new BadRequestHttpException('Missing node "rovers"');
        }

        if (false === is_int($data['mars']['x']) || false === is_int($data['mars']['y'])) {
            throw new InvalidPositionException('Mars coordinates must be int.');
        }

        $mars = new Mars(
            new Coordinates(
                $data['mars']['x'],
                $data['mars']['y'])
        );

        foreach ($data['rovers'] as $roverData) {
            if (false === in_array($roverData['orientation'], Rover::AVAILABLE_ORIENTATIONS)) {
                throw new InvalidOrientationException('Invalid orientation.');
            }

            $rover = new Rover(
                new Coordinates(
                    $roverData['x'],
                    $roverData['y']
                ),
                $roverData['orientation']
            );

            if ($rover->getCoordinates()->getX() > $mars->getCoordinates()->getX() || $rover->getCoordinates()->getY() > $mars->getCoordinates()->getY()) {
                throw new InvalidPositionException('Your rover isn\'t on Mars.');
            }

            $instructions = $roverData['instructions'];
            $instructionsArray = str_split($instructions);

            foreach ($instructionsArray as $instruction) {
                if ("M" === $instruction) {
                    $rover->move();
                } else if (in_array($instruction, Rover::AVAILABLE_MOVES)) {
                    $rover->changeOrientation($instruction);
                } else {
                    throw new InvalidMoveException('Invalid move.');
                }
            }

            if ($rover->getCoordinates()->getX() > $mars->getCoordinates()->getX() || $rover->getCoordinates()->getY() > $mars->getCoordinates()->getY()) {
                throw new InvalidPositionException('Sorry, Your rover felt from Mars.');
            }

            if ($rover->getCoordinates()->getX() < 0 || $rover->getCoordinates()->getY() < 0) {
                throw new InvalidPositionException('Sorry, Your rover felt from Mars.');
            }

            $newRoversPositions = $newRoversPositions . '<br/>' . $rover->__toString();
        }

        return new Response($newRoversPositions, Response::HTTP_OK);
    }

    private function extractData(Request $request): array
    {
        if (null === $request->getContent()) {
            throw new BadRequestHttpException('json payload is empty.');
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        if (null === $data) {
            throw new BadRequestHttpException(
                sprintf(
                    'Unable to decode a json "%s".',
                    $request->getContent()
                )
            );
        }

        return $data;
    }
}
