<?php
declare(strict_types=1);

namespace App\UI\Payload;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ControlRoverPayload
{
    /**
     * @var string|null
     */
    public $mars;

    /**
     * @var string|null
     */
    public $rovers;

    public function __construct(Request $request)
    {
        $rootNode = $this->extractData($request);

        if (!isset($rootNode['mars'])) {
            throw new BadRequestHttpException('Missing node "mars"');
        }

        if (!isset($rootNode['rovers'])) {
            throw new BadRequestHttpException('Missing node "rovers"');
        }

        $this->mars = $rootNode['mars'];
        $this->rovers = $rootNode['rovers'];
    }

    public function resolve()
    {

    }

    private function extractData(Request $request): array
    {
        if (null === $request->getContent()) {
            throw new BadRequestHttpException('json payload is empty.');
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        if (null === $data) {
            throw new BadRequestHttpException(
                sprintf(
                    'Unable to decode a json "%s".',
                    $request->getContent()
                )
            );
        }

        return $data;
    }
}
